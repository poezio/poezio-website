OUTPUT_DIR	=	public/
OUT_DIRS	=	${OUTPUT_DIR}en/ ${OUTPUT_DIR}fr/
IN_DIRS		=	en/ fr/
OUT_IMG_DIR	=	${OUTPUT_DIR}img/
IN_IMG_DIR	=	img/
CSS_DIR		=	css/

all:	directories html css img optimg compress


.PHONY: directories html css img optimg compress

directories:
	mkdir -p ${OUT_DIRS}

img:
	mkdir -p ${OUT_IMG_DIR}
	cp -r ${IN_IMG_DIR}* ${OUT_IMG_DIR}

optimg:
	optipng ${OUT_IMG_DIR}*.png

html:
	LANG=en_US.UTF-8 jinja2_standalone_compiler --path index-fr.html --out ${OUTPUT_DIR}fr/ --settings fr-settings.py
	LANG=en_US.UTF-8 jinja2_standalone_compiler --path index-en.html --out ${OUTPUT_DIR}en/ --settings en-settings.py
	mv ${OUTPUT_DIR}fr/index-fr.html ${OUTPUT_DIR}fr/index.html
	mv ${OUTPUT_DIR}en/index-en.html ${OUTPUT_DIR}en/index.html

clean:
	find . -name \*.gz -delete
	rm -rf ${OUTPUT_DIR}

css:
	cp ${CSS_DIR}normalize.css ${OUTPUT_DIR}
	cp ${CSS_DIR}skeleton.css ${OUTPUT_DIR}

compress:
	find ${OUTPUT_DIR} -type f -exec sh -c "gzip -c --force --best {} > {}.gz"  \;
