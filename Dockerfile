FROM docker.io/fedora:latest

RUN dnf install -y \
   make \
   python3-pip \
   optipng \
   findutils

RUN pip3 install jinja2_standalone_compiler
