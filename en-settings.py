EXTRA_VARIABLES = {
        'about': "About",
        'news': "News",
        'docs': "Docs",
        'support': "Support",
        'faq': "FAQ",
        'bugs': "Bugs",
        'contact': "Contact",
        'download': 'Download',
        'lang': 'en',
        'logo': 'poezio’s logo',
}

