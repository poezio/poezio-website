This repository contains the sources of the poezio website. The
official website is hosted on https://poez.io

Here is an nginx configuration (assuming the sources are placed in
/usr/local/www/poez.io/). Adapt it for your own webserver.

To serve it properly, you’ll need the plugin
https://github.com/giom/nginx_accept_language_module/
and gzip_static (but of course that’s optional)

server
{
        listen       80;
        server_name  poez.io;

        set_from_accept_language $lang en fr;

        gzip_static on;
        gzip_http_version   1.1;
        gzip_vary on;

        location /
        {
            root   /usr/local/www/poez.io/public;
            index  index;

            charset_types application/xhtml+xml;
            default_type application/xhtml+xml;
            charset utf-8;

            rewrite ^/$ $lang/ redirect;
            rewrite ^/(.*)\.php /$1 permanent;
        }
        location /doc
        {
            alias /usr/local/www/poez.io/poezio/doc/build/html;
            index  index.html;
        }
}
server
{
        gzip_static on;
        gzip_http_version   1.1;
        gzip_vary on;

        listen          80;
        server_name     doc.poez.io;
        location /
        {
                root /usr/local/www/poez.io/poezio/doc/build/html;
                index index.html;
                charset utf-8;
        }
}


To build the website with the doc, you’ll need git, python3, python3-sphinx.

The first time or when you changed a file, just do

$ make

and the whole website should be available in public/

You can update the website automatically if it’s on the same server, by adding this to a post-update hook:

DIR=/usr/local/www/poez.io/
GIT_WORK_TREE=$DIR git checkout -f
unset GIT_DIR
cd $DIR && make clean all && cd -
